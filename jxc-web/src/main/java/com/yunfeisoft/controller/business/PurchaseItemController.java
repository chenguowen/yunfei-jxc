package com.yunfeisoft.controller.business;

import com.applet.base.BaseController;
import com.applet.utils.Page;
import com.applet.utils.Response;
import com.applet.utils.ResponseUtils;
import com.yunfeisoft.business.model.PurchaseItem;
import com.yunfeisoft.business.service.inter.PurchaseItemService;
import com.yunfeisoft.model.User;
import com.yunfeisoft.utils.ApiUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * ClassName: PurchaseItemController
 * Description: 采购单商品信息Controller
 * Author: Jackie liu
 * Date: 2020-07-23
 */
@Controller
public class PurchaseItemController extends BaseController {

    @Autowired
    private PurchaseItemService purchaseItemService;

    /**
     * 分页查询采购单商品信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/purchaseItem/list", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response list(HttpServletRequest request, HttpServletResponse response) {
        String productId = ServletRequestUtils.getStringParameter(request, "productId", null);
        User user = ApiUtils.getLoginUser();

        Map<String, Object> params = new HashMap<String, Object>();
        initParams(params, request);
        params.put("productId", productId);
        params.put("orgId", user.getOrgId());

        Page<PurchaseItem> page = purchaseItemService.queryPage(params);
        return ResponseUtils.success(page);
    }
}
