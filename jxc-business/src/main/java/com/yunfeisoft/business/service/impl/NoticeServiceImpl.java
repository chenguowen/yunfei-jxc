package com.yunfeisoft.business.service.impl;

import com.applet.base.BaseServiceImpl;
import com.applet.sql.separation.DataSourceChange;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.NoticeDao;
import com.yunfeisoft.business.model.Notice;
import com.yunfeisoft.business.service.inter.NoticeService;
import com.yunfeisoft.dao.inter.DataDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * ClassName: NoticeServiceImpl
 * Description: 公告信息service实现
 * Author: Jackie liu
 * Date: 2020-08-23
 */
@Service("noticeService")
public class NoticeServiceImpl extends BaseServiceImpl<Notice, String, NoticeDao> implements NoticeService {

    @Autowired
    private DataDao dataDao;

    @Override
    @DataSourceChange(slave = true)
    public Page<Notice> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }

    @Override
    public int modify(Notice notice) {
        int result = super.modify(notice);
        dataDao.saveOrUpdate(notice.getId(), notice.getContent());
        return result;
    }

    @Override
    public int save(Notice notice) {
        int result = super.save(notice);
        dataDao.saveOrUpdate(notice.getId(), notice.getContent());
        return result;
    }
}