package com.yunfeisoft.business.service.inter;

import com.applet.base.BaseService;
import com.applet.utils.Page;
import com.yunfeisoft.business.model.PaymentReceived;

import java.util.Map;

/**
 * ClassName: PaymentReceivedService
 * Description: 收付款信息service接口
 * Author: Jackie liu
 * Date: 2020-08-11
 */
public interface PaymentReceivedService extends BaseService<PaymentReceived, String> {

    public Page<PaymentReceived> queryPage(Map<String, Object> params);
}