package com.yunfeisoft.business.model;

import com.alibaba.excel.annotation.ExcelProperty;
import com.applet.base.ServiceModel;
import com.applet.sql.record.TransientField;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * ClassName: Product
 * Description: 商品信息
 *
 * @Author: Jackie liu
 * Date: 2020-07-23
 */
@Entity
@Table(name = "TT_PRODUCT")
public class Product extends ServiceModel implements Serializable {

    /**
     * Field serialVersionUID: 序列号
     */
    private static final long serialVersionUID = 1L;

    /**
     * 组织id
     */
    @Column
    private String orgId;

    /**
     * 编码
     */
    @ExcelProperty("编码")
    @Column
    private String code;

    /**
     * 名称
     */
    @ExcelProperty("名称")
    @Column
    private String name;

    /**
     * 规格
     */
    @ExcelProperty("规格")
    @Column
    private String standard;

    /**
     * 单位
     */
    @ExcelProperty("单位")
    @Column
    private String unit;

    /**
     * 类别id
     */
    @Column
    private String categoryId;

    /**
     * 供应商id
     */
    @Column
    private String supplierId;

    /**
     * 商品说明
     */
    @ExcelProperty("商品说明")
    @Column
    private String intro;

    /**
     * 进货价
     */
    @ExcelProperty("进货价")
    @Column
    private BigDecimal buyPrice;
    /**
     * 平均进货价
     */
    @ExcelProperty("平均进货价")
    @Column
    private BigDecimal avgPrice;

    /**
     * 零售价
     */
    @ExcelProperty("零售价")
    @Column
    private BigDecimal salePrice;

    /**
     * 会员价
     */
    @ExcelProperty("会员价")
    @Column
    private BigDecimal memberPrice;

    /**
     * 批发价
     */
    @ExcelProperty("批发价")
    @Column
    private BigDecimal tradePrice;

    /**
     * 拼音简码
     */
    @ExcelProperty("拼音简码")
    @Column
    private String pinyinCode;

    @ExcelProperty("供应商")
    @TransientField
    private String supplierName;

    @ExcelProperty("类别")
    @TransientField
    private String categoryName;

    /**
     * 以下3个参数导入时使用
     */
    @ExcelProperty("库存数量")
    @TransientField
    private BigDecimal stock;
    @ExcelProperty("缺货下限")
    @TransientField
    private BigDecimal shortageLimit;
    @ExcelProperty("积压上限")
    @TransientField
    private BigDecimal backlogLimit;

    private List<WarehouseProduct> warehouseProductList;

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStandard() {
        return standard;
    }

    public void setStandard(String standard) {
        this.standard = standard;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public BigDecimal getStock() {
        return stock;
    }

    public void setStock(BigDecimal stock) {
        this.stock = stock;
    }

    public BigDecimal getShortageLimit() {
        return shortageLimit;
    }

    public void setShortageLimit(BigDecimal shortageLimit) {
        this.shortageLimit = shortageLimit;
    }

    public BigDecimal getBacklogLimit() {
        return backlogLimit;
    }

    public void setBacklogLimit(BigDecimal backlogLimit) {
        this.backlogLimit = backlogLimit;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public BigDecimal getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(BigDecimal buyPrice) {
        this.buyPrice = buyPrice;
    }

    public BigDecimal getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(BigDecimal salePrice) {
        this.salePrice = salePrice;
    }

    public BigDecimal getMemberPrice() {
        return memberPrice;
    }

    public void setMemberPrice(BigDecimal memberPrice) {
        this.memberPrice = memberPrice;
    }

    public BigDecimal getTradePrice() {
        return tradePrice;
    }

    public void setTradePrice(BigDecimal tradePrice) {
        this.tradePrice = tradePrice;
    }

    public String getPinyinCode() {
        return pinyinCode;
    }

    public void setPinyinCode(String pinyinCode) {
        this.pinyinCode = pinyinCode;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<WarehouseProduct> getWarehouseProductList() {
        return warehouseProductList;
    }

    public void setWarehouseProductList(List<WarehouseProduct> warehouseProductList) {
        this.warehouseProductList = warehouseProductList;
    }

    public BigDecimal getAvgPrice() {
        return avgPrice;
    }

    public void setAvgPrice(BigDecimal avgPrice) {
        this.avgPrice = avgPrice;
    }
}